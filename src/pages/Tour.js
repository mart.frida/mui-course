import { Box, Typography } from "@mui/material";
import Container from "@mui/material/Container";
import ImageCollage from "../components/ImageCollage";
import CustomizedAccordions from "../components/Accordions";
import Paper from "@mui/material/Paper";
import BottomNavigation from "@mui/material/BottomNavigation";
import BasicModal from "../components/Modal";

export default function Tour() {
  return (
    <Container sx={{ width: 840 }}>
      <Typography variant="h3" component="h1" marginTop={3}>
        Explore the World
      </Typography>
      <Box marginTop={3} sx={{ display: "flex" }} >
        <img
          src="https://cdn.pixabay.com/photo/2016/09/19/07/01/lake-1679708__480.jpg"
          alt="lake"
          height={325}
        ></img>
        <ImageCollage />
      </Box>
      <Typography variant="h6" component="h4" marginTop={3}>
        About this ticket
      </Typography>
      <Box sx={{ display: "flex" }}>
        <Typography variant="paragraph" component="p" marginTop={3}>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sit
          voluptates rem quos delectus debitis earum modi, ipsum veritatis.
          Perferendis officiis nobis, aut hic praesentium nulla vero, possimus
          omnis reprehenderit blanditiis quis incidunt minima voluptatibus illum
          quam corporis libero fugiat doloremque. Lorem ipsum dolor sit amet
          consectetur, adipisicing elit. Exercitationem officiis commodi beatae
          animi incidunt necessitatibus aut provident ad ex. Saepe!
        </Typography>
      </Box>
      <Box marginBottom={10}>
        <Typography variant="h6" component="h4" marginTop={3} marginBottom={2}>
          Frequently asked questions
        </Typography>
        <CustomizedAccordions></CustomizedAccordions>
      </Box>

      <Paper
        sx={{ position: "fixed", bottom: 0, left: 0, right: 0, }}
        elevation={3}
      >
        <BottomNavigation >
          <BasicModal></BasicModal>
        </BottomNavigation>
      </Paper>
      
    </Container>
  );
}
