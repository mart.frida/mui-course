import {createTheme } from '@mui/material/styles';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: '#1976d2',
    },
  },
  text: {
    primary: '#173A5E',
    secondary: '#46505A',
  },
});

export default darkTheme;