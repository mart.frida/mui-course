import * as React from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";

function srcset(image, size, rows = 1, cols = 1) {
  return {
    src: `${image}?w=${size * cols}&h=${size * rows}&fit=crop&auto=format`,
    srcSet: `${image}?w=${size * cols}&h=${
      size * rows
    }&fit=crop&auto=format&dpr=2 2x`,
  };
}

export default function ImageCollage() {
  return (
    <ImageList
      sx={{ width: 300, height: 325, marginLeft: 1 }}
      variant="quilted"
      cols={4}
      rowHeight={121}
    >
      {itemData.map((item) => (
        <ImageListItem
          key={item.img}
          cols={item.cols || 1}
          rows={item.rows || 1}
        >
          <img
            {...srcset(item.img, 121, item.rows, item.cols)}
            alt={item.title}
            loading="lazy"
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
}

const itemData = [
  {
    img: "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510__480.jpg",
    title: "marguerite",
    rows: 2,
    cols: 2,
  },
  {
    img: "https://cdn.pixabay.com/photo/2012/03/01/00/55/flowers-19830__480.jpg",
    title: "flowers",
    cols: 2,
  },

  {
    img: "https://cdn.pixabay.com/photo/2017/02/08/17/24/fantasy-2049567__480.jpg",
    title: "Night",
    cols: 2,
  },
  {
    img: "https://cdn.pixabay.com/photo/2013/07/19/00/18/splashing-165192__480.jpg",
    title: "splashing",
    cols: 2,
  },

  {
    img: "https://cdn.pixabay.com/photo/2014/09/14/18/04/dandelion-445228__480.jpg",
    title: "dandelion",
    rows: 2,
    cols: 2,
  },
  {
    img: "https://cdn.pixabay.com/photo/2016/10/27/22/53/heart-1776746__480.jpg",
    title: "heart",
    cols: 2,
  },
  {
    img: "https://cdn.pixabay.com/photo/2014/01/22/19/44/flower-field-250016__480.jpg",
    title: "flower-field",
    cols: 4,
  },
  {
    img: "https://cdn.pixabay.com/photo/2018/08/12/16/59/parrot-3601194__480.jpg",
    title: "parrot",
    cols: 3,
  },
  {
    img: "https://cdn.pixabay.com/photo/2018/08/23/07/35/thunderstorm-3625405__480.jpg",
    title: "thunderstorm",
  },
  {
    img: "https://cdn.pixabay.com/photo/2016/11/19/18/57/godafoss-1840758__480.jpg",
    title: "godafoss",    
  },
  {
    img: "https://cdn.pixabay.com/photo/2015/11/16/16/28/bird-1045954__480.jpg",
    title: "bird",
    cols: 3,
  }, 
  
];
